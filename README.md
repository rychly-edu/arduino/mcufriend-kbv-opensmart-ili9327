# Using the original MCUFRIEND_kbv for Open-Smart 3.2inch TFT LCD Shield with ILI9327 IC driver

*	There is an Open-Smart overlay and one example in [extras/unused](https://github.com/prenticedavid/MCUFRIEND_kbv/tree/master/extras/unused),
	however, the example does not work (builds, uploads, but does not show anything on Arduino Leonardo); probably different pinout.
*	There are instruction for Open-Smart display is [extras/mcufriend_how_to.txt](https://github.com/prenticedavid/MCUFRIEND_kbv/blob/master/extras/mcufriend_how_to.txt),
	however, they are not showing anything on Arduino Leonardo; probably different pinout.
*	Builds of the original library are to large and could not fit into Arduino Leonardo; it helps to
	*	remove code using [icons.c](https://github.com/prenticedavid/MCUFRIEND_kbv/blob/master/examples/graphictest_kbv/icons.c)
	*	use [Adafruit-GFX-Library](https://github.com/adafruit/Adafruit-GFX-Library/tree/1.7.6)
	*	comment out all `SUPPORT_*` defines in [MCUFRIEND_kbv.cpp](https://github.com/prenticedavid/MCUFRIEND_kbv/blob/master/MCUFRIEND_kbv.cpp), all but `SUPPORT_8352B`

## References

*	[3.2 inch TFT LCD Display module Touch Screen Shield onboard temperature sensor + Pen for Arduino UNO R3/ Mega 2560 R3 / Leonardo](https://www.aliexpress.com/item/3-2-TFT-LCD-Display-module-Touch-Screen-Shield-board-onboard-temperature-sensor-w-Touch-Pen/32755473754.html)
*	[Open-Smart documentation](https://drive.google.com/open?id=0B6uNNXJ2z4CxYktCQlViUkI1Sms) where file `[OPEN-SMART] TFT LCD Shield 3.2inch Update ILI9327.zip` contains
	*	[patched old version of MCUFRIEND_kbv v2.9.1-beta](https://github.com/prenticedavid/MCUFRIEND_kbv/tree/v2.9.2-beta)
	*	[Adafruit GFX Library v1.0.2](https://github.com/adafruit/Adafruit-GFX-Library/tree/v1.0.2)
	*	[an old version of Adafruit_TouchScreen](https://github.com/adafruit/Adafruit_TouchScreen)
	*	[LM75 library](https://github.com/thefekete/LM75)
*	[Issue with Open-Smart 3.2" ILI9327 TFT printing out of margins](https://forum.arduino.cc/t/issue-with-open-smart-3-2-ili9327-tft-printing-out-of-margins-solved/594996/9)
*	[Open-Smart TFT 3.2 and Adruino Due](https://forum.arduino.cc/t/open-smart-tft-3-2-and-adruino-due/498645/38)

# Original README.md from MCUFRIEND_kbv

Library for Uno 2.4, 2.8, 3.5, 3.6, 3.95 inch mcufriend  Shields

1. Install "Adafruit_GFX.h" library from GitHub into your User libraries folder if not already there.

2. Unzip the attached "MCUFRIEND_kbv.zip" into your User libraries folder.

3. Insert your Mcufriend style display shield into UNO.   Only 28-pin shields are supported.

4. Start your Arduino IDE.

5. Build any of the Examples from the File->Examples->Mcufriend_kbv menu.  e.g.

graphictest_kbv.ino: shows all the methods.

LCD_ID_readreg.ino:  diagnostic check to identify unsupported controllers.


HOW TO INSTALL AND USE: is now in "mcufriend_how_to.txt"

CHANGE HISTORY:         is now in "mcufriend_history.txt"
